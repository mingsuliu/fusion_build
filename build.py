#!/usr/bin/env python3
import os
import logging
import yaml
import glob
import shutil
import subprocess
import shlex
import argparse
import re
import sys
import socket
import datetime
import time
import json

from pathlib import Path
from openwrt_builder import OpenWRTPlatformBuilder
from util import bcolors, init_logger

fusion_root_dir = os.getcwd()
fusion_openwrt_dir = '{0}/openwrt'.format(fusion_root_dir)
fusion_images_dir = "{0}/images".format(fusion_root_dir)
#pattern = re.compile(r'\$\{([a-zA-Z]+)\}')
pattern = re.compile(r'\$\{([^}^{]+)\}')
build_ctx = {}

def varexpan_constructor(loader,node):
    value = node.value
    expandvalue = ''
    lastidx = 0

    while(True):
        match = pattern.search(value, lastidx)
        if match is None:
            expandvalue += value[lastidx:]
            break

        if (match.start() != 0):
            expandvalue += value[lastidx:match.start()]

        env_var = match.group()[2:-1]
        expandvalue += build_ctx[env_var]
        lastidx = match.end()

    logging.getLogger().debug("Value {0} expands to {1}".format(value, expandvalue))
    return expandvalue

def load_project(filename):
    #define the regex pattern that the parser will use to 'implicitely' tag your node
    yaml.add_implicit_resolver("!varexpan", pattern)
    yaml.add_constructor('!varexpan', varexpan_constructor)

    with open(filename, 'r') as f:
        return yaml.load(f, Loader=yaml.Loader)

def dump_projects():
    project_images_path = Path(fusion_images_dir)
    projects = list(project_images_path.glob("**/project.yaml"))
    print("\nTarget boards available to build:")
    for _i, project_path in enumerate(projects):
        print("{0} - {1}".format(project_path.parent.name, project_path.parent))
    return

def create_platform_builder(project):
    return OpenWRTPlatformBuilder()

def save_build_ctx_to_file():
    with open('{0}/.build_ctx.json'.format(fusion_root_dir), 'w') as outfile:
        json.dump(build_ctx, outfile, indent=4)

def build_projects(args, projects):
    for _i, project_path in enumerate(projects):
        p = load_project(str(project_path))
    
        build_ctx['project_def'] = p
        images_def = {}
        for _i, image in enumerate(p['images']):
            images_def[image['name']] = image

        if 'releases' in p:
            images_releases = p['releases']
        else:
            images_releases = None

        platform_builder = create_platform_builder(p)
        build_ctx['project'] = project_path.parent.name
        build_ctx['project_path'] = str(project_path.parent)

        if args.clean:
            pipeline = platform_builder.create_clean_pipeline()
            pipeline.execute(build_ctx)
            break

        logging.getLogger().info("Found a project {0} from {1} and start building".
            format(project_path.parent.name, fusion_images_dir))

        for _i, build in enumerate(p['builds']):
            if args.build != None:
                if args.build != build['name']:
                    continue
            else:
                if 'default' in build:
                    if build['default'] is False:
                        continue

            logging.getLogger().info("Start to build for {0}".format(build['name']))
            pipeline = platform_builder.create_build_pipeline(build)
            if 'models' in p:
                build_ctx['project_models'] = p['models']
            build_ctx['project_image_list'] = p['images']
            build_ctx['project_parameters'] = p['parameters']
            build_ctx['project_build'] = build
            pipeline.execute(build_ctx)

            for _i, image in enumerate(build['images']):
                pack_image_pipeline = platform_builder.create_pack_image_pipeline(images_def[image])
                pack_image_pipeline.execute(build_ctx)

            if args.releasedir != None and images_releases != None:
                for _i, release in enumerate(images_releases):
                    release_image_pipeline = platform_builder.create_release_image_pipeline(release)
                    release_image_pipeline.execute(build_ctx)

def main():
    global fusion_root_dir
    global fusion_openwrt_dir
    global fusion_images_dir

    curr_root_dir = os.path.dirname(os.path.abspath(sys.argv[0]))
    if curr_root_dir != fusion_root_dir:
        fusion_root_dir = curr_root_dir
        fusion_openwrt_dir = '{0}/openwrt'.format(fusion_root_dir)
        fusion_images_dir = "{0}/images".format(fusion_root_dir)

    init_logger('build.log')

    parser = argparse.ArgumentParser(description='Fusion build frontend system')
    parser.add_argument('--target', dest='target', action='store',
                        help='specify the target board to build for. The target board is defined under images directory.')
    parser.add_argument('--build', dest='build', action='store',
                        help='To specify the build from the project settings')
    parser.add_argument('--buildno', dest='buildno', action='store', type=int, default=1,
                        help='To specify the build number')
    parser.add_argument('--release-dir', dest='releasedir', action='store', default='./bin',
                        help='To specicy the release directory')
    parser.add_argument('--clean', dest='clean', action='store_true', default=False,
                        help='Clean the intermediate files of a build')
    parser.add_argument('--jobs', dest='jobs', action='store', type=int, default=5,
                        help='Specifies  the  number  of  jobs  (commands)  to run simultaneously.')
    parser.add_argument('--verbose', dest='verbose', action='store_true', default=False,
                        help='Enable the verbose mode.')

    args = parser.parse_args()
    
    logging.getLogger().info("{0}Fusion build frontend system is running from {1}{2}".format(bcolors.HEADER, fusion_root_dir, bcolors.ENDC))

    project_images_path = Path(fusion_images_dir)
    if args.target is None:
        projects = list(project_images_path.glob("**/project.yaml"))
    else:
        projects = list(project_images_path.glob("{0}/**/project.yaml".format(args.target)))

    if (len(projects) == 0):
        dump_projects()
        return

    build_ctx['root_dir'] = fusion_root_dir
    build_ctx['openwrt_dir'] = fusion_openwrt_dir
    build_ctx['images_dir'] = fusion_images_dir
    build_ctx['jobs'] = args.jobs
    build_ctx['verbose'] = args.verbose
    build_ctx['buildno'] = "{:0>3d}".format(args.buildno)
    build_ctx['hostname'] = socket.gethostname()
    build_ctx['buildtime'] = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d.%H%M%S')

    if os.path.isabs(args.releasedir):
        build_ctx['releasedir'] = args.releasedir
    else:
        build_ctx['releasedir'] = '{0}/{1}'.format(fusion_root_dir, args.releasedir)

    build_projects(args, projects)
    save_build_ctx_to_file()

if __name__ == '__main__':
    main()
