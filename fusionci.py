#!/usr/bin/env python3
import argparse
import logging
import os
import json
import yaml
import xml.etree.ElementTree as ET
from pygerrit2 import GerritRestAPI, HTTPBasicAuth, HTTPDigestAuth
from util import run_command_with_logging, init_logger

_cookies = None
_fusion_manifest_path = "http://172.16.10.244:8080/Fusion/manifest"

def get_change_info(rest, change):
    global _cookies

    '''
    if _cookies is not None:
        gerritChange = rest.get("/changes/{0}/detail/?o=CURRENT_REVISION".
        format(change), False, cookies=_cookies)
    else:
        gerritChange, rawResponse =
            rest.get("/changes/{0}/detail/?o=CURRENT_REVISION"
                .format(change), return_response=True)
        if _cookies is None:
            _cookies = rawResponse.cookies
    '''
    gerritChange = \
        rest.get("/changes/{0}/detail/?o=CURRENT_REVISION".format(change))
    return gerritChange

def load_projects_def(filename):
    with open(filename, 'r') as f:
        return yaml.load(f)


def download_projects_manifest(projects_def):
    for _i, project in enumerate(projects_def['projects']):
        #print(project['name'])
        os.system("git clone {0}/{1} --branch {2} --single-branch {3}".
            format(_fusion_manifest_path,
                   project['project'], project['branch'], project['name']))

        project['manifest'] = dict()
        tree = ET.parse('{0}/default.xml'.format(project['name']))
        root = tree.getroot()
        for child in root:
            if child.tag == 'project':
                project['manifest'][child.attrib['name']] = child.attrib

def mark_ci_projects(rest, change, projects_def, username):
    review_message = \
        "Fusion CI will run build check for the following projects: "

    gerritChange = get_change_info(rest, change)
    print("Change[{0}] is for {1} on branch {2}".
        format(change, gerritChange['project'], gerritChange['branch']))

    for _i, project in enumerate(projects_def['projects']):
        project_manifest = project['manifest']
        #print(change['project'])
        if gerritChange['project'] in project_manifest:
            #print(project_manifest['revision'])
            revision = project_manifest[gerritChange['project']]['revision']
            #print(revision)
            found = revision.find(gerritChange['branch'])
            if found != -1:
                project['changes'].append(change)
                review_message += project['project'] + "/" + project['branch']
                review_message += " "

    review_change(rest, gerritChange, review_message, 0, username)

def generate_fusion_ci_projects(projects_def):
    fusion_ci_projects = dict()

    fusion_ci_projects['projects'] = list()
    for _i, project in enumerate(projects_def['projects']):
        project_ci = dict()
        project_ci['name'] = project['name']
        project_ci['product'] = project['project']
        project_ci['branch'] = project['branch']

        changes = ''
        for _i, change in enumerate(project['changes']):
            changes += "{0} ".format(change)

        project_ci['changes'] = changes.rstrip()
        fusion_ci_projects['projects'].append(project_ci)

    with open('fusion_projects.yaml', 'w') as f:
        yaml.dump(fusion_ci_projects, f, default_flow_style=False)

def list_projects_from_changes(rest, changes, ciprojectsdef, username):
    projects_def = load_projects_def(ciprojectsdef)
    for _i, project in enumerate(projects_def['projects']):
        project['changes'] = list()

    download_projects_manifest(projects_def)
    #for key in projects_def['projects']['PortalS_master']['manifest']:
    #print("[{0}".format(str(projects_def['projects'])))

    for _i, change in enumerate(changes):
        mark_ci_projects(rest, change, projects_def, username)

    '''
    for _i, project in enumerate(projects_def['projects']):
        if len(project['changes']):
            print("Project {0} is required to run CI: {1}".
                format(project['name'], project['changes']))
    '''
    generate_fusion_ci_projects(projects_def)

def download_changes(rest, changes):
    res = 0
    for _i, change in enumerate(changes):
        revision = 1
        gerritChange = get_change_info(rest, change)

        #print(json.dumps(gerritChange, sort_keys=True))
        if 'current_revision' in gerritChange:
            revisionId = gerritChange['current_revision']
            revision = gerritChange['revisions'][revisionId]['_number']

        print("Download change from gerrit {0}/{1}".format(change, revision))
        res = os.system("repo download -c {0} {1}/{2}".
                format(gerritChange['project'], change, revision))
        if res != 0:
            break

    return res

def gerrit_change_find_latest_change_id(gerritChange, id):
    return gerritChange['revisions'][id]['_number']

'''
def decide_score(gerritChange, reviewScore, username):
    last_score = 0
    for label in  gerritChange['labels']['Code-Review']['all']:
        if label['username'] != username:
            continue

        logging.getLogger().info(
            "Found my last review score = {0}".format(label['value']))

        last_score = label['value']
        break

    if last_score != -1:
        return reviewScore

    change_id = gerrit_change_find_latest_change_id(
        gerritChange, gerritChange['current_revision'])

    for message in gerritChange['messages']:
        #logging.getLogger().debug(message)
        if message['_revision_number'] != change_id:
            continue

        if message['real_author']['username'] != username:
            continue

        logging.getLogger().info(message['message'].strip())

    return -1
'''

def review_change(rest, gerritChange, reviewMessage, reviewScore, username):
    global _cookies
    score = reviewScore

    '''
    if 'Code-Review' in gerritChange['labels'] and \
       'all' in gerritChange['labels']['Code-Review']:
       score = decide_score(gerritChange, score, username)
    '''

    gerritReviewMsg = {'labels':{}}
    gerritReviewMsg['message'] = reviewMessage
    gerritReviewMsg['labels']['Code-Review'] = score

    #logging.getLogger().debug(json.dumps(gerritChange, indent=4))
    rest.post("/changes/{0}/revisions/{1}/review".
        format(gerritChange['id'], gerritChange['current_revision']),
            data=gerritReviewMsg, cookies=_cookies)

def review_changes(rest, changes, reviewMessage, reviewScore, username):
    for _i, change in enumerate(changes):
        gerritChange = get_change_info(rest, change)
        review_change(rest, gerritChange, reviewMessage.lstrip(),
                      reviewScore, username)

def init_argument_parser():
    parser = argparse.ArgumentParser(description='Fusion CI')
    parser.add_argument('--changes', nargs='+', dest='changes', action='store',
                        type=int,
                        help='specify the target board to build for. \
                              The target board is defined \
                              under images directory.')
    parser.add_argument('--download', dest='download', action='store_true',
                        default=False,
                        help='Download the changes from the Gerrit server.')
    parser.add_argument('--download-patch', dest='download_patch',
                        action='store_true', default=False,
                        help='Download the changes from the Gerrit server \
                              as patch files.')
    parser.add_argument('--review', dest='review', action='store_true',
                        default=False, help='Review the changes.')
    parser.add_argument('--review-message', dest='reviewMessage',
                        action='store', default="", help='Review message')
    parser.add_argument('--review-score', dest='reviewScore', action='store',
                        default=0, type=int, help='Review score')
    parser.add_argument('--ci-projects', dest='ciprojects', action='store_true',
                        default=False,
                        help='List projects to run CI for changes.')
    parser.add_argument('--ci-projects-def', dest='ciprojectsdef',
                        action='store', default="",
                        help='The projects definition YAML file.')
    parser.add_argument('--gerrit-username', dest='gerrit_username',
                        action='store', default="", required=True,
                        help='The user name for the Gerrit server')
    parser.add_argument('--gerrit-passwd', dest='gerrit_passwd',
                        action='store', default="", required=True,
                        help='The password for the Gerrit server')
    parser.add_argument('--product', dest='product', action='store', default="",
                        help='The product to download source code from.')
    parser.add_argument('--branch', dest='branch', action='store', default="",
                        help='The branch name.')
    parser.add_argument('--workspace', dest='workspace', default="workspace",
                        help='Workspace.')
    return parser


def main():
    init_logger('fusionci.log')

    res = 0

    parser = init_argument_parser()
    args = parser.parse_args()

    print("Gerrit changes: {0}".format(args.changes))

    auth = HTTPBasicAuth(args.gerrit_username, args.gerrit_passwd)
    rest = GerritRestAPI(url='http://172.16.10.244:8080', auth=auth)

    rest.login("http://172.16.10.244:8080/login")
    if args.download:
        if len(args.product) != 0:
            run_command_with_logging("mkdir {0}".format(args.workspace))
            os.chdir(args.workspace)
            run_command_with_logging("repo init -u {0}/{1} -b {2}".
                format(_fusion_manifest_path, args.product, args.branch))
            run_command_with_logging("repo sync -j8")

        if args.changes != None:
            res = download_changes(rest, args.changes)

        if res != 0:
            print("CI download changes are failed")
            res = -1
    elif args.ciprojects:
        list_projects_from_changes(rest, args.changes,
            args.ciprojectsdef, args.gerrit_username)
    elif args.review:
        review_changes(rest, args.changes, args.reviewMessage,
            args.reviewScore, args.gerrit_username)
    else:
        parser.print_help()

    print("res: {0}".format(res))
    if res != 0:
        exit(res)
    else:
        exit(0)

if __name__ == '__main__':
    main()
