import os
import logging
import yaml
import glob
import shutil
import subprocess
import shlex
import argparse
import re
import sys
import time
import filecmp
from shutil import copyfile
from pathlib import Path
from util import run_command_with_logging

OPENWRT_STAGING_HOST_BIN = "staging_dir/host/bin"
OPENWRT_FEEDS_UPDATE_COMMAND = "./scripts/feeds update -a"
OPENWRT_FEEDS_INSTALL_COMMAND = "./scripts/feeds install -a -f"
OPENWRT_DEFAULT_MAKE_COMMAND = "make"
OPENWRT_DEFAULT_CLEAN_COMMAND = "make distclean"
OPENWRT_BUILD_ERROR_MESSAGE = "build failed. Please re-run make with -j1 V=s to see what's going on"

class OpenWRTBuildPipeline:
    def __init__(self, build):
        self.build_tasks = [self.build_task_init, self.build_task_feeds_init, self.build_task_run, self.build_task_images_pack]
        self.build = build
        return

    def copy_config(self, source_file, dest_file):
        try:
            if filecmp.cmp(source_file, dest_file):
                return
            logging.info("Copy config from {0} to {1} ".format(source_file, dest_file))
        except FileNotFoundError:
            logging.debug("Config file {0} is not existed so copying a new one".format(dest_file))

        shutil.copyfile(source_file, dest_file)
        return

    def build_task_init(self, build_ctx):
        project_path = Path(build_ctx['project_path'])
        build = build_ctx['project_build']

        feeds_conf = Path("{0}/{1}".format(str(project_path), build['feeds']))
        defconfig = Path("{0}/{1}".format(str(project_path), build['defconfig']))
        kconfig = Path("{0}/{1}".format(str(project_path), build['kconfig']))
        #logging.info("Start to build for {0} with feeds {1} defconfig {2}".format(build['name'], feeds_conf, defconfig))

        feeds_conf_dest = Path("{0}/feeds.conf".format(str(build_ctx['openwrt_dir'])))
        self.copy_config(str(feeds_conf), str(feeds_conf_dest))

        defconfig_dest = Path("{0}/.config".format(str(build_ctx['openwrt_dir'])))
        self.copy_config(str(defconfig), str(defconfig_dest))

        for line in open(str(defconfig_dest), 'r'):
            if line.startswith('CONFIG_TARGET_BOARD'):
                build_ctx['openwrt_target'] = line.strip().split('=')[1].strip('"')
            if line.startswith('CONFIG_TARGET_SUBTARGET'):
                build_ctx['openwrt_subtarget'] = line.strip().split('=')[1].strip('"')
                break

        # If overlay is assigned, check whether symbolic link is correct
        if 'overlay' in build:
            overlay_symlink = Path("{0}/overlay/{1}".format(str(build_ctx['openwrt_dir']), str(build_ctx['openwrt_target'])))

            if overlay_symlink.is_symlink():
                linkpath = str(overlay_symlink.resolve())
                dirname = os.path.dirname(linkpath)
                basename = os.path.basename(linkpath)

                if basename != build['overlay']:
                    os.unlink(str(overlay_symlink))
                    os.symlink(build['overlay'], "{0}/{1}".format(dirname, build_ctx['openwrt_target']))

        kconfig_dest_file = build['kconfig'].replace("config/", "")
        kconfig_dest_file = kconfig_dest_file.replace(build_ctx['project'] + '.', "")
        kconfig_dest = Path("{0}/target/linux/{1}/{2}".format(str(build_ctx['openwrt_dir']),
                                                                  str(build_ctx['openwrt_target']),
                                                                  str(kconfig_dest_file)))
        kconfig_exists = False
        logging.debug(kconfig_dest)
        if os.path.exists(str(kconfig_dest)):
            kconfig_exists = True

        if not kconfig_exists:
            kconfig_dest = Path("{0}/target/linux/{1}/{2}/{3}".format(str(build_ctx['openwrt_dir']),
                                                                    str(build_ctx['openwrt_target']),
                                                                    str(build_ctx['openwrt_subtarget']),
                                                                    str(kconfig_dest_file)))
        logging.debug(kconfig_dest)         

        self.copy_config(str(kconfig), str(kconfig_dest))

        logging.info("OpenWRT target: {0}".format(build_ctx['openwrt_target']))
        logging.info("OpenWRT feeds config: {0}".format(feeds_conf))
        logging.info("OpenWRT defconfig: {0}".format(defconfig))

        #inject version before run
        for _i, image in enumerate(build_ctx['project_image_list']):
            if image['name'] == 'firmware':
                firmware = image

        if 'ubnizecfg' in build:
            ubinizecfg = Path("{0}/{1}".format(str(project_path), build['ubnizecfg']))
            ubinizecfg_dest_file = build['ubnizecfg'].replace("config/", "")
            ubinizecfg_dest = Path("{0}/target/linux/{1}/image/{2}".format(str(build_ctx['openwrt_dir']),
                                                             str(build_ctx['openwrt_target']),
                                                             str(ubinizecfg_dest_file)))

            self.copy_config(str(ubinizecfg), str(ubinizecfg_dest))

        with open('{0}/fwinfo'.format(build_ctx['openwrt_dir']), 'w') as f:
            f.write('{0},{1}'.format(firmware['version'],
            time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())))
        return 0

    def build_task_feeds_init(self, build_ctx):
        os.chdir(build_ctx['openwrt_dir'])
        feeds_dir = Path("{0}/feeds".format(build_ctx['openwrt_dir']))
        if (feeds_dir.exists()):
            return 0

        ret = run_command_with_logging(OPENWRT_FEEDS_UPDATE_COMMAND)
        logging.info("Feeds update with ret {0}".format(ret))

        ret = run_command_with_logging(OPENWRT_FEEDS_INSTALL_COMMAND)
        logging.info("Feeds install with ret {0}".format(ret))
        return 0

    def build_task_run(self, build_ctx):
        project_path = build_ctx['project_path']
        build = build_ctx['project_build']
        parameters = build_ctx['project_parameters'][0]

        os.chdir(build_ctx['openwrt_dir'])
        defconfig = Path("{0}/{1}".format(str(project_path), build['defconfig']))
        defconfig_dest = Path("{0}/.config".format(str(build_ctx['openwrt_dir'])))
        shutil.copyfile(str(defconfig), str(defconfig_dest))

        if 'jobs' in parameters:
            build_cmd = "{0} -j{1}".format(OPENWRT_DEFAULT_MAKE_COMMAND, parameters['jobs'])
        else:
            build_cmd = "{0} -j{1}".format(OPENWRT_DEFAULT_MAKE_COMMAND, build_ctx['jobs'])
        if build_ctx['verbose']:
            build_cmd += " V=s"

        logging.info("OpenWRT build command: {0}".format(build_cmd))
        ret = run_command_with_logging(build_cmd)
        if ret and not build_ctx['verbose']:
            logging.info("OpenWRT build is failed! Re-build with make -j1 V=s")
            run_command_with_logging("make -j1 V=s")
            '''
                It's likely "make -j1 V=s" will compile OK however we'd like to return
                error to break the build
            '''
            return ret
        else:
            return 0

    def build_task_images_pack_generic(self, build_ctx):
        return 0

    def build_task_images_pack_qsdk(self, build_ctx):
        project_path = build_ctx['project_path']
        
        os.chdir(build_ctx['root_dir'])

        openwrt_bin_path = Path('{0}/bin/{1}'.format(build_ctx['openwrt_dir'], build_ctx['openwrt_target']))
        logging.debug(openwrt_bin_path)
        images_bin_path = Path('{0}/common/build/ipq_x64'.format(project_path))
        files = list(openwrt_bin_path.glob("**/openwrt*"))

        dtbs_path = Path('{0}/bin/{1}/dtbs'.format(build_ctx['openwrt_dir'], build_ctx['openwrt_target']))
        logging.debug(dtbs_path)
        files_dtbs = list(dtbs_path.glob("**/*"))

        ubinize_exists = False
        ubinize_bin_path = Path('{0}/staging_dir/host/bin/ubinize'.format(build_ctx['openwrt_dir']))
        logging.debug(ubinize_bin_path)
        if os.path.exists(str(ubinize_bin_path)):
            ubinize_exists = True

        for _i, file in enumerate(files):
            dest_file = "{0}/{1}".format(images_bin_path, file.name)
            logging.debug("Copy openwrt bin file {0} to {1}".format(str(file), dest_file))
            shutil.copyfile(str(file), dest_file)
        
        for _i, file in enumerate(files_dtbs):
            dest_file = "{0}/{1}".format(images_bin_path, file.name)
            logging.debug("Copy dtbs file {0} to {1}".format(str(file), dest_file))
            shutil.copyfile(str(file), dest_file)

        if (ubinize_exists):
            dest_file = "{0}/{1}".format(images_bin_path, os.path.basename(str(ubinize_bin_path)))
            logging.debug("Copy ubinize bin file {0} to {1}".format(str(ubinize_bin_path), dest_file))
            shutil.copy2(str(ubinize_bin_path), dest_file)

        return 0

    def build_task_images_pack(self, build_ctx):
        logging.debug("Enter _task_images_pack")

        images_pack_dispatch = {'MTK11AX_REF': self.build_task_images_pack_generic,
                                'WAX650S_REF': self.build_task_images_pack_qsdk}

        images_pack_dispatch[build_ctx['project']](build_ctx)

        return 0

    def execute(self, build_ctx):
        build_ok = True
        for _i, task in enumerate(self.build_tasks):
            ret = task(build_ctx)
            if ret is not 0:
                build_ok = False
                break

        if build_ok:
            logging.info("Build is successful")
        else:
            logging.error("Build is failed! Please check the build.log for the details")
            sys.exit(-1)
        return build_ok

class OpenWRTPackMTKFirmwarePipeline:
    def __init__(self, image):
        self.image = image
        return

    def execute(self, build_ctx):
        logging.info("Enter OpenWRTPackMTKFirmwarePipeline")
        project_path = build_ctx['project_path']
        firmware = self.image
        parameters = build_ctx['project_parameters'][0]

        if 'sysupgrade' in parameters:
            sysupgrade_filename = parameters['sysupgrade']
        else:
            print("Sysupgrade filename isn't configurated")
            return False

        output_path = "{0}/bin/".format(project_path)
        if not os.path.exists(output_path):
            os.makedirs(output_path)

        openwrt_bin_path = Path("{0}/bin/targets/{1}/{2}".format(str(build_ctx['openwrt_dir']),
                                                                str(build_ctx['openwrt_target']),
                                                                str(build_ctx['openwrt_subtarget'])))
        files = list(openwrt_bin_path.glob("**/{0}".format(str(sysupgrade_filename))))
        if not files:
            logging.debug("No {0}is found from ".format(str(sysupgrade_filename)) + str(openwrt_bin_path))
            return False
        for _i, file in enumerate(files):
            dest_file = "{0}/{1}".format(output_path, file.name)
            logging.debug("Move openwrt bin file {0} to {1}".format(str(file), dest_file))
            shutil.move(str(file), dest_file)

        return

class OpenWRTPackQSDKFirmwarePipeline:
    def __init__(self, image):
        self.image = image
        return

    def execute(self, build_ctx):
        os.chdir('{0}/common/build'.format(build_ctx['project_path']))
        return run_command_with_logging("python update_common_info.py")

class OpenWRTCleanPipeline:
    def execute(self, build_ctx):
        os.chdir(build_ctx['openwrt_dir'])
        return run_command_with_logging(OPENWRT_DEFAULT_CLEAN_COMMAND)

class OpenWRTReleasePipeline:
    def __init__(self, release):
        self.release = release
        return

    def execute(self, build_ctx):
        logging.debug("Enter OpenWRTReleasePipeline execute")
        project_path = Path(build_ctx['project_path'])
        logging.debug("Enter OpenWRTReleasePipeline execute " + str(project_path))
        files = list(project_path.glob("**/{0}".format(self.release['from_file'])))

        if (len(files) != 1):
            logging.debug("File {0} is not found".format(self.release['from_file']))
            return False

        base_path = Path(build_ctx['releasedir'])
        to_file_path = base_path / self.release['to_file']
        md5_file_path = to_file_path.with_suffix('.md5')

        os.makedirs(str(base_path), exist_ok=True)
        copyfile(str(files[0]), str(to_file_path))
        md5 = subprocess.check_output(['md5sum', self.release['to_file']], cwd=str(base_path))
        with open(str(md5_file_path), 'wb') as outfile:
            outfile.write(md5)
        return True

class ArchiveReleasePipeline:
    def __init__(self, release):
        self.release = release
        return

    def execute(self, build_ctx):
        logging.debug("Enter ArchiveReleasePipeline execute")
        subprocess.check_output(['tar', 'zcvf', self.release['to_file']] + self.release['from_file'], cwd=build_ctx['releasedir'])
        return True

class OpenWRTPlatformBuilder:
    def __init__(self):
        return

    def create_build_pipeline(self, build):
        return OpenWRTBuildPipeline(build)

    def create_pack_image_pipeline(self, image):
        if not 'type' in image:
            logging.error("Invalid image definition for " + image['name'])
            return None

        if image['type'] == 'squashfs-mtk':
            return OpenWRTPackMTKFirmwarePipeline(image)
        else:
            return OpenWRTPackQSDKFirmwarePipeline(image)

    def create_release_image_pipeline(self, release):
        if release['image'] == 'archive':
            return ArchiveReleasePipeline(release)
        else:
            return OpenWRTReleasePipeline(release)

    def create_clean_pipeline(self):
        return OpenWRTCleanPipeline()
